# Data Layer Definitions

## Background

### Motivation

Surface transaction information to Google Tag Manger (GTM) so that GTM can route information to relevant endpoints (
Google Analytics, Google Ads, Facebook, etc.)

### Methodology

A tag management system like Google Tag Manager (GTM) is useful to:

- Maintain consistent logic for tracking website events to various marketing vendors
    - Can control sequencing, priority, and error handling in one place
- Minimizing the dev request queue for tracking interactions

For GTM to be comprehensively useful for the above, a data layer must be populated with all the necessary data for the
various marketing vendors. This document will outline adjustments to the data layer to support this in the future.

## Snippets

GTM creates a global JS array “dataLayer” when it is instantiated. Pushing objects to that array allow GTM to observe
the data and route it to the desired locations.

- [Click to Buy](ClickToBuyDataLayer.html)
    - This code should render when a click to purchase from a third-party is executed.
    - Note that the `eventCallback` function should contain the navigation action code. Any link click should be handled
      with `event.preventDefault()`, then the dataLayer push shoudl happen, then the eventCallback contains the routine
      to navigate. This prevents race conditions between JS execution and page unload.
- [Transaction](TransactionDataLayer.html)
    - This code should executed after a successful onsite transaction is recorded.
